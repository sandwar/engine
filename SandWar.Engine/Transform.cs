using System;
using System.Collections.Generic;
using SandWar.Math;
using System.Linq;

namespace SandWar.Engine
{
	/// <summary>This is an object that has a position inside the simulation.</summary>
	public sealed class Transform : ScriptableComponent
	{
		/// <summary>Parent of this transform.</summary>
		public Transform Parent {
			get { return _parent; }
			set {
				if(_parent == value)
					return;

				if(_parent != null)
					_parent.children.Remove(this);

				if(value != null && !value.children.Contains(this))
					value.children.Add(this);

				_parent = value;

				Transform parent = value;
				while(parent != null) {
					parent = parent.Parent;
					if (parent == value)
						throw new RecursiveReferenceException("Trying to add a transform to itself");
				}
			}
		}
		private Transform _parent;

		#region Matrix operations

		// TODO: make position, rotation and scale setters update matrix accordingly.
		private Vector3 _position;
		private Quaternion _rotation;
		private Vector3 _scale;

		/// <summary>Position of this transform relative to parent.</summary>
		public Vector3 Position {
			get { return _position; }
			set { _position = value; }
		}

		/// <summary>Rotation of this transform relative to parent.</summary>
		public Quaternion Rotation {
			get { return _rotation; }
			set { _rotation = value; }
		}

		/// <summary>Scale of this transform.</summary>
		public Vector3 Scale {
			get { return _scale; }
			set { _scale = value; }
		}

		/// <summary>The transformation matrix of this transform.</summary>
		public Matrix4 Matrix { get; private set; }

		#endregion

		/// <summary>Children of this transform.</summary>
		public IList<Transform> Children {
			get { return children; }
		}
		internal readonly TransformChildren children; 

		/// <summary>Components of this transform.</summary>
		public IList<ScriptableComponent> Components {
			get { return components; }
		}
		internal readonly TransformComponents components; 

		/// <summary>Construct an empty transform at origin.</summary>
		public Transform() {
			children = new TransformChildren(this);
			components = new TransformComponents(this);
		}

		#region GetComponents and similar

		/// <summary>Get components of the specified type.</summary>
		public IEnumerable<T> GetComponents<T>() where T : ScriptableComponent {
			return components.OfType<T>();
		}

		/// <summary>Get the first component of the specified type or null if not found.</summary>
		public T GetComponent<T>() where T : ScriptableComponent {
			return GetComponents<T>().FirstOrDefault();
		}

		/// <summary>Get components of the specified type searching in children too.</summary>
		public IEnumerable<T> GetComponentsInChildren<T>() where T : ScriptableComponent {
			return components.OfType<T>().Concat(
				children.SelectMany(transform => transform.GetComponentsInChildren<T>())
			);
		}

		/// <summary>Get the first component of the specified type searching in children too, or null if not found.</summary>
		public T GetComponentInChildren<T>() where T : ScriptableComponent {
			return GetComponentsInChildren<T>().FirstOrDefault();
		}

		#endregion

		#region Event handlers

		public override void OnFrame() {
			foreach(var component in Components)
				if(component.Enabled)
					component.OnFrame();

			foreach(var child in Children)
				if(child.Enabled)
					child.OnFrame();
		}

		public override void OnPhysics() {
			foreach(var component in Components)
				if(component.Enabled)
					component.OnPhysics();

			foreach(var child in Children)
				if(child.Enabled)
					child.OnPhysics();
		}

		#endregion
	}
}

