using System;
using System.Collections.Generic;
using System.Collections;

namespace SandWar.Engine
{
	internal class TransformComponents : IList<ScriptableComponent>
	{
		private readonly Transform transform;
		private readonly List<ScriptableComponent> list = new List<ScriptableComponent>();

		public TransformComponents(Transform transform) {
			this.transform = transform;
		}

		#region IList implementation

		public int IndexOf(ScriptableComponent item) {
			return list.IndexOf(item);
		}

		public void Insert(int index, ScriptableComponent item) {
			list.Insert(index, item);
			item.Transform = transform;
		}

		public void RemoveAt(int index) {
			var component = list[index];
			component.Transform = null;
			//list.RemoveAt(index); // not required
		}

		public ScriptableComponent this[int index] {
			get { return list[index]; }
			set {
				if(list.Count > index) {
					var previous = list[index];

					if (previous == value)
						return;

					previous.Transform = null;
				}

				list[index] = value;
				value.Transform = transform;
			}
		}

		#endregion

		#region ICollection implementation

		public void Add(ScriptableComponent item) {
			list.Add(item);
			item.Transform = transform;
		}

		public void Clear() {
			for(var i = list.Count - 1; i >= 0; i--)
				list[i].Transform = null;

			//list.Clear(); // not required
		}

		public bool Contains(ScriptableComponent item) {
			return list.Contains(item);
		}

		public void CopyTo(ScriptableComponent[] array, int arrayIndex) {
			throw new InvalidOperationException("A "+typeof(ScriptableComponent).Name+" cannot be inside multiple "+typeof(Transform).Name+" instances at once.");
		}

		public bool Remove(ScriptableComponent item) {
			var retval = list.Remove(item);

			if(retval)
				item.Transform = null;

			return retval;
		}

		public int Count {
			get { return list.Count; }
		}

		public bool IsReadOnly {
			get { return false; }
		}

		#endregion

		#region IEnumerable implementation

		public IEnumerator<ScriptableComponent> GetEnumerator() {
			return list.GetEnumerator();
		}

		#endregion

		#region IEnumerable implementation

		IEnumerator IEnumerable.GetEnumerator() {
			return ((IEnumerable)list).GetEnumerator();
		}

		#endregion
	}
}
