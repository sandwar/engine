using System;
using System.Collections.Generic;
using System.Collections;

namespace SandWar.Engine
{
	internal class TransformChildren : IList<Transform>
	{
		private readonly Transform parent;
		private readonly List<Transform> list = new List<Transform>();

		public TransformChildren(Transform parent) {
			this.parent = parent;
		}

		#region IList implementation

		public int IndexOf(Transform item) {
			return list.IndexOf(item);
		}

		public void Insert(int index, Transform item) {
			list.Insert(index, item);
			item.Parent = parent;
		}

		public void RemoveAt(int index) {
			var transform = list[index];
			transform.Parent = null;
			//list.RemoveAt(index); // not required
		}

		public Transform this[int index] {
			get { return list[index]; }
			set {
				if(list.Count > index) {
					var previous = list[index];

					if (previous == value)
						return;

					previous.Parent = null;
				}

				list[index] = value;
				value.Parent = parent;
			}
		}

		#endregion

		#region ICollection implementation

		public void Add(Transform item) {
			list.Add(item);
			item.Parent = parent;
		}

		public void Clear() {
			for(var i = list.Count - 1; i >= 0; i--)
				list[i].Parent = null;

			//list.Clear(); // not required
		}

		public bool Contains(Transform item) {
			return list.Contains(item);
		}

		public void CopyTo(Transform[] array, int arrayIndex) {
			throw new InvalidOperationException("A "+typeof(Transform).Name+" cannot have multiple "+typeof(Transform).Name+" parents at once.");
		}

		public bool Remove(Transform item) {
			var retval = list.Remove(item);

			if(retval)
				item.Parent = null;

			return retval;
		}

		public int Count {
			get { return list.Count; }
		}

		public bool IsReadOnly {
			get { return false; }
		}

		#endregion

		#region IEnumerable implementation

		public IEnumerator<Transform> GetEnumerator() {
			return list.GetEnumerator();
		}

		#endregion

		#region IEnumerable implementation

		IEnumerator IEnumerable.GetEnumerator() {
			return ((IEnumerable)list).GetEnumerator();
		}

		#endregion
	}
}
