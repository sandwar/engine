using System;

namespace SandWar.Engine
{
	public struct Color
	{
		public byte R;
		public byte G;
		public byte B;
		public byte A;

		public Color(byte r, byte g, byte b) : this(r, g, b, 0) { }

		public Color(byte r, byte g, byte b, byte a) : this(r, g, b) {
			this.R = r;
			this.G = g;
			this.B = b;
			this.A = 0;
		}
	}
}

