using System;
using SandWar.Engine.Graphics;

namespace SandWar.Engine
{
	public class MeshRenderer : ScriptableComponent
	{
		public Mesh Mesh;

		public MeshRenderer() {
		}

		public override void OnFrame() {
			if(!Mesh.built)
				Mesh.mesh.Build();

			Mesh.mesh.Render();
		}
	}
}

