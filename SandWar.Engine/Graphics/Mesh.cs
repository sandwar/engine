using System;
using SandWar.Math;
using SandWar.Engine.Rendering;

namespace SandWar.Engine.Graphics
{
	/// <summary>In a virtual object, the mesh is the shape of the object.</summary>
	public class Mesh
	{
		/// <summary>Bounding box around the mesh.</summary>
		public Bounds Bounds { get; private set; }

		/// <summary>Position of mesh vertices.</summary>
		public Vector3[] Vertices {
			get { return mesh.Vertices; }
			set {
				mesh.Vertices = value;
				built = false;
			}
		}

		/// <summary>Triangles of the mesh (triplets of vertice indices).</summary>
		public int[] Triangles {
			get { return mesh.Triangles; }
			set {
				mesh.Triangles = value;
				built = false;
			}
		}

		/// <summary>Colors of mesh vertices.</summary>
		public Color[] Colors { get; set; }

		/// <summary>Normals of mesh vertices.</summary>
		public Vector3[] Normals { get; set; }

		/// <summary>UV of mesh vertices.</summary>
		public Vector2[] UV { get; set; }

		internal RenderizableMesh mesh;
		internal bool built = false;

		/// <summary>Construct an empty mesh.</summary>
		public Mesh() {
			mesh = new RenderizableMesh();
		}
	}
}
