using System;

namespace SandWar.Engine
{
	/// <summary>This is an object that can respond to inputs from a simulation.</summary>
	public class ScriptableComponent
	{
		/// <summary>Enables this script.</summary>
		public bool Enabled {
			get { return _enabled; }
			set { _enabled = value; }
		}
		private bool _enabled = true;

		/// <summary>Transform of this component.</summary>
		public Transform Transform {
			get { return _transform; }
			internal set {
 				if(_transform == value)
					return;

				if(_transform != null)
					_transform.components.Remove(this);

				if(value != null && !value.components.Contains(this))
					value.components.Add(this);

				_transform = value;
			}
		}
		private Transform _transform;

		/// <summary>Called before every frame is rendered.</summary>
		public virtual void OnFrame() { }

		/// <summary>Called multiple times per-frame, between physics simulation ticks.</summary>
		public virtual void OnPhysics() { }
	}
}

