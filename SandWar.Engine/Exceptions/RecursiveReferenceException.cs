using System;

namespace SandWar.Engine
{
	/// <summary>Setting a reference would cause a recursion.</summary>
	public class RecursiveReferenceException : InvalidOperationException
	{
		public RecursiveReferenceException() : base("Recursive reference detected") { }
		public RecursiveReferenceException(string message) : base(message) { }
		public RecursiveReferenceException(string message, Exception innerException) : base(message, innerException) { }
	}
}

