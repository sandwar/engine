using System;
using System.Collections.Generic;

namespace SandWar.Engine
{
	public class Scene
	{
		/// <summary>Transforms in this scene.</summary>
		public readonly List<Transform> Transforms = new List<Transform>();

		/// <summary>Construct an empty scene.</summary>
		public Scene() {
		}
	}
}

