using System;
using System.Threading.Tasks;
using System.Threading;
using SandWar.Engine.Rendering;

namespace SandWar.Engine
{
	/// <summary>An engine capable of running a simulation.</summary>
	public class Engine
	{
		/// <summary>The task for this engine.</summary>
		public readonly Task Task;

		/// <summary>The scene currently loaded in this engine.</summary>
		public Scene Scene { get; set; }

		private readonly RenderingWindow render;

		/// <summary>Construct a headless engine instance.</summary>
		public Engine() {
			this.Task = new Task(task);
		}

		/// <summary>Construct an engine instance with the specified rendering context.</summary>
		public Engine(RenderingWindow render) : this() {
			this.render = render;
		}

		private void task() {
			while(true) {
				foreach (var transform in Scene.Transforms)
					transform.OnFrame();

				if(render != null) {
					render.SwapBuffers();
				}

				Thread.Sleep(100);	// test
			}
		}
	}
}

