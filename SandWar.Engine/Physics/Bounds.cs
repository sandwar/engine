using System;
using SandWar.Math;

namespace SandWar.Engine
{
	/// <summary>Represents an axis aligned bounding box.</summary>
	public struct Bounds
	{
		private Vector3 _min, _max;

		/// <summary>Minimum point in space of this bounds.</summary>
		public Vector3 Min {
			get { return _min; }
			set {
				_min = value;

				if(_min.X > _max.X)
					swap(ref _min.X, ref _max.X);

				if(_min.Y > _max.Y)
					swap(ref _min.Y, ref _max.Y);

				if(_min.Z > _max.Z)
					swap(ref _min.Z, ref _max.Z);
			}
		}

		/// <summary>Maximum point in space of this bounds.</summary>
		public Vector3 Max {
			get { return _max; }
			set {
				_max = value;

				if(_min.X > _max.X)
					swap(ref _min.X, ref _max.X);

				if(_min.Y > _max.Y)
					swap(ref _min.Y, ref _max.Y);

				if(_min.Z > _max.Z)
					swap(ref _min.Z, ref _max.Z);
			}
		}

		/// <summary>Size of the bounds, double of the extents.</summary>
		public Vector3 Size {
			get { return (Max - Min).Absolute; }
			set { Extents = value * .5f; }
		}

		/// <summary>Extents of the bounds, half of the size.</summary>
		public Vector3 Extents {
			get { return Size * .5f; }
			set {
				var center = Center;
				var extents = value.Absolute;
				Min = center - extents;
				Max = center + extents;
			}
		}

		/// <summary>Center of the bounds.</summary>
		public Vector3 Center {
			get { return Min + Extents; }
			set {
				var oldCenter = Center;
				var newCenter = value;
				var offset = newCenter - oldCenter;

				Min += offset;
				Max += offset;
			}
		}

		private static void swap<T>(ref T a, ref T b) {
			T buff = a;
			a = b;
			b = buff;
		}
	}
}

