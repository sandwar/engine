using System;
using SandWar.Math;

namespace SandWar.Engine.Test
{
	public class TestMotion : ScriptableComponent
	{
		private float num = 0f;

		public TestMotion() {
		}

		public override void OnFrame() {
			Transform.Position = new Vector3(Transform.Position.X, Math.Math.Sin(num), Transform.Position.Z);
			num += 0.05f;
		}
	}
}

