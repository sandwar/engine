using System;
using System.Drawing;
using SandWar.Engine;
using SandWar.Engine.Graphics;
using SandWar.Engine.Rendering;
using SandWar.Math;

namespace SandWar.Engine.Test
{
	public class Program
	{
		/*public SimpleWindow() : base(800, 600)
		{
			KeyDown += Keyboard_KeyDown;
		}

		#region Keyboard_KeyDown

		/// <summary>
		/// Occurs when a key is pressed.
		/// </summary>
		/// <param name="sender">The KeyboardDevice which generated this event.</param>
		/// <param name="e">The key that was pressed.</param>
		void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
		{
			if (e.Key == Key.Escape)
				this.Exit();

			if (e.Key == Key.F11)
				if (this.WindowState == WindowState.Fullscreen)
					this.WindowState = WindowState.Normal;
			else
				this.WindowState = WindowState.Fullscreen;
		}

		#endregion

		#region OnLoad

		/// <summary>
		/// Setup OpenGL and load resources here.
		/// </summary>
		/// <param name="e">Not used.</param>
		protected override void OnLoad(EventArgs e)
		{
			GL.ClearColor(Color.MidnightBlue);
		}

		#endregion

		#region OnResize

		/// <summary>
		/// Respond to resize events here.
		/// </summary>
		/// <param name="e">Contains information on the new GameWindow size.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnResize(EventArgs e)
		{
			GL.Viewport(0, 0, Width, Height);

			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);
		}

		#endregion

		#region OnUpdateFrame

		/// <summary>
		/// Add your game logic here.
		/// </summary>
		/// <param name="e">Contains timing information.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			// Nothing to do!
		}

		#endregion

		#region OnRenderFrame

		/// <summary>
		/// Add your game rendering code here.
		/// </summary>
		/// <param name="e">Contains timing information.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.Begin(PrimitiveType.Triangles);

			GL.Color3(Color.MidnightBlue);
			GL.Vertex2(-1.0f, 1.0f);
			GL.Color3(Color.SpringGreen);
			GL.Vertex2(0.0f, -1.0f);
			GL.Color3(Color.Ivory);
			GL.Vertex2(1.0f, 1.0f);

			GL.End();

			this.SwapBuffers();
		}

		#endregion*/

		[STAThread]
		public static void Main()
		{
			Transform cube = new Transform();
			cube.Components.Add(new TestMotion());
			cube.Components.Add(new MeshRenderer() {
				Mesh = new Mesh() {
					Vertices = new Vector3[] {
						new Vector3(-1.0f, -1.0f,  1.0f),
						new Vector3( 1.0f, -1.0f,  1.0f),
						new Vector3( 1.0f,  1.0f,  1.0f),
						new Vector3(-1.0f,  1.0f,  1.0f),
						new Vector3(-1.0f, -1.0f, -1.0f),
						new Vector3( 1.0f, -1.0f, -1.0f), 
						new Vector3( 1.0f,  1.0f, -1.0f),
						new Vector3(-1.0f,  1.0f, -1.0f)
					},
					Triangles = new int[] {
						// front face
						0, 1, 2, 2, 3, 0,
						// top face
						3, 2, 6, 6, 7, 3,
						// back face
						7, 6, 5, 5, 4, 7,
						// left face
						4, 0, 3, 3, 7, 4,
						// bottom face
						0, 1, 5, 5, 4, 0,
						// right face
						1, 5, 6, 6, 2, 1
					}
				}
			});

			using(var window = new RenderingWindow()) {
				var engine = new Engine(window);
				engine.Scene = new Scene();

				engine.Scene.Transforms.Add(cube);
				engine.Task.RunSynchronously();
			}

			/*using (SimpleWindow example = new SimpleWindow())
			{
				// Get the title and category  of this example using reflection.
				//Utilities.SetWindowTitle(example);
				example.Run(30.0, 0.0);
			}*/
		}
	}
}
