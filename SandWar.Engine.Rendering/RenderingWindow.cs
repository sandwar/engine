using System;
using OpenTK;
using System.Drawing;
using System.Reflection;
using OpenTK.Graphics.OpenGL;

namespace SandWar.Engine.Rendering
{
	public class RenderingWindow : IDisposable
	{
		private readonly GameWindow window;

		/// <summary>Enter or exit fullscreen mode.</summary>
		public bool Fullscreen {
			get { return window.WindowState == WindowState.Fullscreen; }
			set {
				if(value) {
					_lastWindowState = window.WindowState;
					window.WindowState = WindowState.Fullscreen;
				} else {
					window.WindowState = _lastWindowState == WindowState.Fullscreen ? WindowState.Normal : _lastWindowState;
				}
			}
		}
		private WindowState _lastWindowState;

		public RenderingWindow() {
			window = new GameWindow();

			try {
				window.Icon = Icon.ExtractAssociatedIcon(Assembly.GetEntryAssembly().Location);
			} catch(Exception) {
				window.Icon = null;
			}

			try {
				window.Title = ((AssemblyTitleAttribute)Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title;
			} catch(Exception) {
				window.Title = "";
			}

			window.Resize += (s, e) => GL.Viewport(0, 0, window.Width, window.Height);

			// Check for necessary capabilities:
			Version version = new Version(GL.GetString(StringName.Version).Substring(0, 3));
			Version target = new Version(2, 0);
			if(version < target)
				throw new NotSupportedException("OpenGL "+target+" required, only "+version+" available");

			GL.ClearColor(Color.MidnightBlue);
			test_init();

			RenderingProgram prog = new RenderingProgram(Shaders.Fragment);
			prog.Use();

			//test_frame(cubeIndices, vbuff, elbuff);

			window.Visible = true;
		}

		public void SwapBuffers() {
			window.SwapBuffers();

			// TEST START
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			Matrix4 lookat = Matrix4.LookAt(0, 5, 5, 0, 0, 0, 0, 1, 0);
			GL.MatrixMode(MatrixMode.Modelview);
			GL.LoadMatrix(ref lookat);
			// TEST END
		}

		/// <summary>[requires: v1.0][deprecated: v3.2]</summary>
		public void test_init() {
			float aspect_ratio = window.Width / (float)window.Height;
			Matrix4 perpective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspect_ratio, 1, 64);
			GL.MatrixMode(MatrixMode.Projection);
			GL.LoadMatrix(ref perpective);

			/*GL.MatrixMode(MatrixMode.Projection);
			GL.LoadIdentity();
			GL.Ortho(-1.0, 1.0, -1.0, 1.0, 0.0, 4.0);*/
		}

		public void Dispose() {
			window.Dispose();
		}
	}
}
