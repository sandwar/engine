using System;
using SandWar.Math;
using OpenTK.Graphics.OpenGL;

namespace SandWar.Engine.Rendering
{
	/// <summary>Contains data that can be rendered on the screen.</summary>
	public class RenderizableMesh : IDisposable
	{
		/// <summary>Position of mesh vertices.</summary>
		public Vector3[] Vertices = EmptyArray.Vector3;

		/// <summary>Triangles of the mesh (triplets of vertice indices).</summary>
		public int[] Triangles = EmptyArray.Int32;

		/// <summary>Colors of mesh vertices.</summary>
		//public Color[] Colors;	// TODO

		/// <summary>Normals of mesh vertices.</summary>
		public Vector3[] Normals = EmptyArray.Vector3;

		/// <summary>UV of mesh vertices.</summary>
		public Vector2[] UV = EmptyArray.Vector2;

		private bool buffersGenerated = false;
		private GLBuffer vertex, element;

		public RenderizableMesh() { }

		/// <summary>Compose this mesh and send it to the GPU.</summary>
		public void Build() {
			if(!buffersGenerated) {
				int buffid;
				GL.GenBuffers(1, out buffid);
				vertex = buffid;
				/*GL.GenBuffers(1, out color_buffer_object);*/
				GL.GenBuffers(1, out buffid);
				element = buffid;

				buffersGenerated = true;
			}

			int size;

			// Upload the vertex buffer.
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertices.Length * 3 * sizeof(float)), Vertices, BufferUsageHint.StaticDraw);
			GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
			if(size != Vertices.Length * 3 * sizeof(Single))
				throw new OutOfVideoMemoryException("Uploaded "+size+" bytes of "+(Vertices.Length * 3 * sizeof(float))+" required");

			// Upload the color buffer.
			/*GL.BindBuffer(BufferTarget.ArrayBuffer, color_buffer_object);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(shape.Colors.Length * sizeof(int)), shape.Colors,
			              BufferUsageHint.StaticDraw);
			GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
			if (size != shape.Colors.Length * sizeof(int))
				throw new ApplicationException(String.Format(
					"Problem uploading vertex buffer to VBO (colors). Tried to upload {0} bytes, uploaded {1}.",
					shape.Colors.Length * sizeof(int), size));*/

			// Upload the index buffer (elements inside the vertex buffer, not color indices as per the IndexPointer function!)
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, element);
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(Triangles.Length * sizeof(int)), Triangles, BufferUsageHint.StaticDraw);
			GL.GetBufferParameter(BufferTarget.ElementArrayBuffer, BufferParameterName.BufferSize, out size);
			if(size != Triangles.Length * sizeof(int))
				throw new OutOfVideoMemoryException("Uploaded "+size+" bytes of "+(Triangles.Length * sizeof(int))+" required");
		}

		/// <summary>Render this mesh.</summary>
		public void Render() {
			GL.EnableClientState(ArrayCap.VertexArray);
			//GL.EnableClientState(ArrayCap.ColorArray);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex);
			GL.VertexPointer(3, VertexPointerType.Float, 0, IntPtr.Zero);
			/*GL.BindBuffer(BufferTarget.ArrayBuffer, color_buffer_object);
			GL.ColorPointer(4, ColorPointerType.UnsignedByte, 0, IntPtr.Zero);*/
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, element);

			GL.DrawElements(PrimitiveType.Triangles, Triangles.Length, DrawElementsType.UnsignedInt, IntPtr.Zero);

			//GL.DrawArrays(GL.Enums.BeginMode.POINTS, 0, shape.Vertices.Length);

			GL.DisableClientState(ArrayCap.VertexArray);
		}

		public void Dispose() {
			if(buffersGenerated) {
				GL.DeleteBuffer(vertex);
				GL.DeleteBuffer(element);

				buffersGenerated = false;
			}
		}
	}
}

