using System;
using OpenTK.Graphics.OpenGL;

namespace SandWar.Engine.Rendering
{
	/// <summary>Thrown when a shader fails to be compiled.</summary>
	public class ShaderCompileFailedException : ApplicationException
	{
		/// <summary>Status code of the compiler.</summary>
		public readonly int Status;

		/// <summary>Type of the shader.</summary>
		public readonly string Type;

		/// <summary>Source code of the shader.</summary>
		public readonly string Shader;

		/// <summary>Error message from the compiler.</summary>
		public readonly string Error;

		public ShaderCompileFailedException() : base("Failed to compile a shader") { }
		public ShaderCompileFailedException(string message) : base(message) { }
		public ShaderCompileFailedException(string message, Exception innerException) : base(message, innerException) { }
		public ShaderCompileFailedException(int statusCode, string shaderType, string shaderSource, string errorMessage) : this() {
			Status = statusCode;
			Type = shaderType;
			Shader = shaderSource;
			Error = errorMessage;
		}
	}
}

