using System;
using OpenTK.Graphics.OpenGL;

namespace SandWar.Engine.Rendering
{
	/// <summary>Thrown when a program fails to be linked.</summary>
	public class ProgramLinkFailedException : ApplicationException
	{
		/// <summary>Status code of the compiler.</summary>
		public readonly int Status;

		/// <summary>Error message from the compiler.</summary>
		public readonly string Error;

		public ProgramLinkFailedException() : base("Failed to link a program") { }
		public ProgramLinkFailedException(string message) : base(message) { }
		public ProgramLinkFailedException(string message, Exception innerException) : base(message, innerException) { }
		public ProgramLinkFailedException(int statusCode, string errorMessage) : this() {
			Status = statusCode;
			Error = errorMessage;
		}
	}
}

