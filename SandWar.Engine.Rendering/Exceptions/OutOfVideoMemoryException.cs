using System;

namespace SandWar.Engine.Rendering
{
	/// <summary>Thrown when graphics card gets out of memory.</summary>
	public class OutOfVideoMemoryException : OutOfMemoryException
	{
		public OutOfVideoMemoryException() : base("Graphics card out of memory") { }
		public OutOfVideoMemoryException(string message) : base(message) { }
		public OutOfVideoMemoryException(string message, Exception innerException) : base(message, innerException) { }
	}
}

