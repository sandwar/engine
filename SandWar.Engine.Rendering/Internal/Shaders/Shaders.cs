using System;
using System.IO;
using System.Reflection;

namespace SandWar.Engine.Rendering
{
	internal static class Shaders
	{
		public static string Fragment {
			get { return readShader("Fragment.glsl"); }
		}

		public static string Vertex {
			get { return readShader("Vertex.glsl"); }
		}

		private static string readShader(string res) {
			using(var stream = Assembly.GetAssembly(typeof(Shaders)).GetManifestResourceStream("Shaders/"+res))
			using(var sr = new StreamReader(stream))
				return sr.ReadToEnd();
		}
	}
}

