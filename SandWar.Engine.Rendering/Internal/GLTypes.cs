using System;

namespace SandWar.Engine.Rendering
{
	internal struct GLShader
	{
		private readonly int ID;

		private GLShader(int id) {
			ID = id;
		}

		public static implicit operator GLShader(int id) {
			return new GLShader(id);
		}

		public static implicit operator int(GLShader o) {
			return o.ID;
		}
	}

	internal struct GLProgram
	{
		private readonly int ID;

		private GLProgram(int id) {
			ID = id;
		}

		public static implicit operator GLProgram(int id) {
			return new GLProgram(id);
		}

		public static implicit operator int(GLProgram o) {
			return o.ID;
		}
	}

	internal struct GLBuffer
	{
		private readonly int ID;

		private GLBuffer(int id) {
			ID = id;
		}

		public static implicit operator GLBuffer(int id) {
			return new GLBuffer(id);
		}

		public static implicit operator int(GLBuffer o) {
			return o.ID;
		}
	}
}

