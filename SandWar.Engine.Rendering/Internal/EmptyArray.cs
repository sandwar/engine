using System;
using SandWar.Math;

namespace SandWar.Engine.Rendering
{
	internal static class EmptyArray
	{
		public static int[] Int32 = new int[0];

		public static Vector2[] Vector2 = new Vector2[0];
		public static Vector3[] Vector3 = new Vector3[0];
	}
}

