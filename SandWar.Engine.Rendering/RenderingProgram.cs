using System;
using OpenTK.Graphics.OpenGL;
using SandWar.Logging;

namespace SandWar.Engine.Rendering
{
	/// <summary>Holds a rendering program.</summary>
	public class RenderingProgram : IDisposable
	{
		private readonly GLProgram program;

		public RenderingProgram(string fragmentShader) {
			var vertex = createShader(ShaderType.VertexShader, Shaders.Vertex);
			var fragment = createShader(ShaderType.FragmentShader, fragmentShader);

			program = createProgram(vertex, fragment);

			GL.DeleteShader(vertex);
			GL.DeleteShader(fragment);
		}

		/// <summary>Use this rendering program.</summary>
		public void Use() {
			GL.UseProgram(program);
		}

		public void Dispose() {
			GL.DeleteProgram(program);
		}

		/// <summary>[requires: v2.0]</summary>
		private static GLShader createShader(ShaderType shaderType, string shaderSource) {
			var shader = GL.CreateShader(shaderType);
			GL.ShaderSource(shader, shaderSource);
			GL.CompileShader(shader);

			string info;
			int statusCode;
			GL.GetShaderInfoLog(shader, out info);
			GL.GetShader(shader, ShaderParameter.CompileStatus, out statusCode);

			if(statusCode != 1)
				throw new ShaderCompileFailedException(statusCode, shaderType.ToString(), shaderSource, info);
		
			return shader;
		}

		/// <summary>[requires: v2.0]</summary>
		private static GLProgram createProgram(params GLShader[] shaders) {
			var program = GL.CreateProgram();

			foreach(var shader in shaders)
				GL.AttachShader(program, shader);

			GL.LinkProgram(program);

			string info;
			int statusCode;
			GL.GetProgramInfoLog(program, out info);
			GL.GetProgram(program, GetProgramParameterName.LinkStatus, out statusCode);

			if(statusCode != 1)
				throw new ProgramLinkFailedException(statusCode, info);

			return program;
		}
	}
}

