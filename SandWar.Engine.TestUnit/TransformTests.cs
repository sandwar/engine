using NUnit.Framework;
using System;

namespace SandWar.Engine.TestUnit
{
	[TestFixture]
	public class TransformTests
	{
		[Test]
		public void InheritanceExplicitTest() {
			var a = new Transform();
			var b = new Transform();
			var c = new Transform();

			a.Parent = b;
			Assert.AreEqual(b, a.Parent, "A's parent is B");
			Assert.That(b.Children.Contains(a), "A is child of B");

			a.Parent = c;
			Assert.AreEqual(c, a.Parent, "A's parent is C");
			Assert.That(!b.Children.Contains(a), "A is not child of B");
			Assert.That(c.Children.Contains(a), "A is child of C");

			a.Parent = null;
			Assert.IsNull(a.Parent, "A has no parent");
			Assert.That(!b.Children.Contains(a), "A is not child of B");
			Assert.That(!c.Children.Contains(a), "A is not child of C");
		}

		[Test]
		public void InheritanceExplicitRecursionSimpleTest() {
			var a = new Transform();
			var b = new Transform();

			a.Parent = b;
			Assert.Throws<RecursiveReferenceException>(() => b.Parent = a);
		}

		[Test]
		public void InheritanceExplicitRecursionDeepTest() {
			var a = new Transform();
			var b = new Transform();
			var c = new Transform();

			a.Parent = b;
			b.Parent = c;
			Assert.Throws<RecursiveReferenceException>(() => c.Parent = a);
		}

		[Test]
		public void InheritanceListTest() {
			var a = new Transform();
			var b = new Transform();
			var c = new Transform();

			b.Children.Add(a);
			Assert.AreEqual(b, a.Parent, "A's parent is B");
			Assert.That(b.Children.Contains(a), "A is child of B");

			c.Children.Add(a);
			Assert.AreEqual(c, a.Parent, "A's parent is C");
			Assert.That(!b.Children.Contains(a), "A is not child of B");
			Assert.That(c.Children.Contains(a), "A is child of C");

			c.Children.Remove(a);
			Assert.IsNull(a.Parent, "A has no parent");
			Assert.That(!b.Children.Contains(a), "A is not child of B");
			Assert.That(!c.Children.Contains(a), "A is not child of C");
		}

		[Test]
		public void InheritanceListRecursionSimpleTest() {
			var a = new Transform();
			var b = new Transform();

			b.Children.Add(a);
			Assert.Throws<RecursiveReferenceException>(() => a.Children.Add(b));
		}

		[Test]
		public void InheritanceListRecursionDeepTest() {
			var a = new Transform();
			var b = new Transform();
			var c = new Transform();

			b.Children.Add(a);
			c.Children.Add(b);
			Assert.Throws<RecursiveReferenceException>(() => a.Children.Add(c));
		}
	}
}

